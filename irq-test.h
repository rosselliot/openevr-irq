/* Register Map Offsets */
#define EVR_STATUS_REG       0x0000
#define EVR_CTRL_REG         0x0004
#define EVR_IRQ_FLAG_REG     0x0008
#define EVR_IRQ_EN_REG       0x000c
#define EVR_DATABUF_CTRL_REG 0x0020
#define ESS_STATUS_REG       0xFFF0
#define ESS_CTRL_REG         0xFFF4

/* Irq Enable/Flag register bit masks */
#define IRQ_RX_VIOL         0x01
#define IRQ_FIFO_FULL       0x02
#define IRQ_HEARTBEAT       0x04
#define IRQ_EVENT           0x08
#define IRQ_HARDWARE        0x10
#define IRQ_DATABUF         0x20
#define IRQ_LINK_CHANGE     0x40
#define IRQ_SEG_DATABUF     0x80
#define IRQ_SEQ_START       0x100
#define IRQ_SEQ_STOP        0x1000
#define IRQ_SEQ_HALFWAY     0x10000
#define IRQ_SEQ_OVERFLOW    0x100000
#define IRQ_EN              0x80000000

/* Databuffer Control and status bit masks */
#define DATABUF_CTRL_STOP 0x4000
