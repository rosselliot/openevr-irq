obj-m       := irq-test.o
ccflags-y   := -I$(src)

all: modules

modules modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$(PWD) $@

clean:
	rm -f *.o .depend *.ko *.mod.c modules.order Module.symvers .*.*.cmd
