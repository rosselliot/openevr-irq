/*
 *  Simple kernel module to test the Irq signal from openEVR
 *
 */

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include "irq-test.h"

#define DRIVER_NAME "openevr-irq"
#define DEVICE_NAME "openevr-irq"

struct openevr_irq_device {
    void __iomem *regs;
    struct device *dev;
    int irq;
    struct resource *res;
    unsigned int handled;
    unsigned int missed_heartbeat_cnt;
    unsigned int rx_error_cnt;
};

static int openevr_enable_irqs(void *data)
{
    struct openevr_irq_device *odev = data;
    u32 enFlag = 0;

    enFlag = IRQ_EN | IRQ_EVENT | IRQ_HEARTBEAT | IRQ_RX_VIOL;

    writel(enFlag, odev->regs + EVR_IRQ_EN_REG);
    dev_info(odev->dev, "Interrupts enabled\n");

    return 0;
}

static int openevr_disable_irqs(void *data)
{
    struct openevr_irq_device *odev = data;

    writel(0, odev->regs + EVR_IRQ_EN_REG);
    dev_info(odev->dev, "Interrupts disabled\n");

    return 0;
}

static void openevr_bitset(void *data, u32 offset, u32 val) {

    struct openevr_irq_device *odev = data;
    u32 regRead = 0;

    regRead = readl(odev->regs + offset);
    writel(regRead & val, odev->regs + offset);

}

static irqreturn_t openevr_ctrl_irq(int irq, void *data)
{

    struct openevr_irq_device *odev = data;
    u32 regData = 0;

    /* Read IrqFlag register */
    regData = readl(odev->regs + EVR_IRQ_FLAG_REG);
    dev_info(odev->dev, "IRQ: triggered (0x%.8x)\n", regData);

    /* Handle interrupts here */
    if (regData & IRQ_RX_VIOL) {
       odev->rx_error_cnt++;
       dev_info(odev->dev, "IRQ: RxViolation [%d]\n", odev->rx_error_cnt);
    }
    if (regData & IRQ_FIFO_FULL) {
        dev_info(odev->dev, "IRQ: FIFO Full\n");
        /* Drain FIFO here */
    }
    if (regData & IRQ_HEARTBEAT) {
        odev->missed_heartbeat_cnt++;
        dev_info(odev->dev, "IRQ: HeartBeat [%d]\n", odev->missed_heartbeat_cnt);
    }
    if (regData & IRQ_EVENT ) {
        dev_info(odev->dev, "IRQ: Event\n");
    }
    if (regData & IRQ_HARDWARE) {
    /* Not sure what happens with this one... */
    }
    if (regData & IRQ_DATABUF) {
        /* Stop the databuffer */
        openevr_bitset(odev, EVR_DATABUF_CTRL_REG, DATABUF_CTRL_STOP);
        /* Drain the databuffer... */
        dev_info(odev->dev, "IRQ: Databuffer full\n");
    }
    /* Write back to IrqFlag register to clear flags */
    writel(regData, odev->regs + EVR_IRQ_FLAG_REG);

    /* Increment interrupt counter */
    odev->handled++;
    dev_info(odev->dev, "IRQ: cleared (0x%.8x) [%d]\n", regData, odev->handled);

    /* After 10 interrupts have been received, disable */
    if (odev->handled >= 10) {
        /* Disable interrupts */
        dev_info(odev->dev, "IRQ: 10 interrupts received. Disabling to prevent spamming system log\n");
        openevr_disable_irqs(odev);
    }

    return IRQ_RETVAL(1);
}


static int openevr_irq_probe(struct platform_device *op)
{
    struct openevr_irq_device *odev;
    struct resource res;
	int err;

    odev = kzalloc(sizeof(*odev), GFP_KERNEL);
    if (!odev) {
        err = -ENOMEM;
        goto out_return;
    }

    odev->dev = &op->dev;

    err = of_address_to_resource(op->dev.of_node, 0, &res);
    if (err) {
        dev_err(&op->dev, "unable to get register resources from devicetree\n");
        goto out_free;
    }

    //if(!request_mem_region(res.start, resource_size(&res), "openEVR")) {
    //    dev_err(&op->dev, "unable to register memory segment\n");
    //    err = -EBUSY;
    //    goto out_free;
    //}

    odev->regs = of_iomap(op->dev.of_node, 0);
    if (!odev->regs) {
        dev_err(&op->dev, "unable to map ioregmap registers\n");
        err = -ENOMEM;
        goto out_free;
    }

    odev->res = &res;

    /* map the channel IRQ if it exists, but don't hookup the handler yet */
    odev->irq = irq_of_parse_and_map(op->dev.of_node, 0);

    /* Hookup the IRQ handler*/
    err = request_irq(odev->irq, openevr_ctrl_irq, IRQF_SHARED, "openevr-controller", odev);
    if(err) {
        dev_err(odev->dev, "unable to request IRQ [%d]\n", err);
        goto out_free_odev;
    }

    platform_set_drvdata(op, odev);

    /* Enable interrupts */
    openevr_enable_irqs(odev);

    return 0;

out_free_odev:
    irq_dispose_mapping(odev->irq);
    iounmap(odev->regs);
out_free:
    kfree(odev);
out_return:
    return err;
}

static int openevr_irq_remove(struct platform_device *op)
{
    struct openevr_irq_device *odev;
    odev = platform_get_drvdata(op);

    /* Disable interrupts */
    openevr_disable_irqs(odev);

    free_irq(odev->irq, odev);

    //release_mem_region(odev->res->start, resource_size(odev->res));

    iounmap(odev->regs);
	kfree(odev);

    return 0;
}

static const struct of_device_id openevr_irq_id[] = {
    { .compatible = "ess,openevr-1.00.a", },
    { }
};
MODULE_DEVICE_TABLE(of, openevr_irq_id);

static struct platform_driver openevr_irq_driver = {
    .driver = {
        .name = DRIVER_NAME,
        .of_match_table = openevr_irq_id,
    },
    .probe = openevr_irq_probe,
    .remove = openevr_irq_remove,
};

/*----------------------------------------------------------------------------*/
/* Module Init / Exit                                                         */
/*----------------------------------------------------------------------------*/
static __init int openevr_irq_init(void)
{
    pr_info("ESS openEVR interrupt test module\n");
    return platform_driver_register(&openevr_irq_driver);
}

static void __exit openevr_irq_exit(void)
{
    return platform_driver_unregister(&openevr_irq_driver);
}

subsys_initcall(openevr_irq_init);
module_exit(openevr_irq_exit);

MODULE_DESCRIPTION("Simple module to test the openEVR Irq line");
MODULE_LICENSE("GPL v2");
